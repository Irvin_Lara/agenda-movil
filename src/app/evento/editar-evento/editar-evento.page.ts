import { Component, OnInit } from '@angular/core';
import { EventoService } from '../../services/evento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-evento',
  templateUrl: './editar-evento.page.html',
  styleUrls: ['./editar-evento.page.scss'],
})
export class EditarEventoPage implements OnInit {

  evento = {
    id_evento: null,
    id_usuario: null,
    nombre_evento: null,
    fecha_evento: null,
    hora_inicio_evento: null,
    hora_fin_evento: null
  }

  evento2 = {
    idEvento: null,
    idUsuario: null,
    nombreEvento: null,
    fechaEvento: null,
    horaInicioEvento: null,
    horaFinEvento: null
  }

  constructor(private eventoService: EventoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      //alert("hola");
      this.obtenerEvento();
    }
  }

  obtenerEvento() {
    this.eventoService.obtenerEvento1(window.localStorage.getItem("idEvento")).subscribe(
      (data:any)=> {this.evento = data.evento;},
      (error)=> {console.log(error);}
    );
  }

  editarEvento() {
    this.evento.fecha_evento = this.evento.fecha_evento.substring(0, 10);
    this.evento.id_usuario = window.localStorage.getItem("idUsuario");
    this.asignarArreglo();
    this.eventoService.actualizarEvento1(this.evento2).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Evento actualizado correctamente");
          this.limpiarArreglos();
          this.router.navigate(['/tabs/tab2']);
        } else if (datos['estado'] == 2) {
          alert("Registro incorrecto");
        }
      }
    );
  }

  asignarArreglo() {
    this.evento2.idEvento = this.evento.id_evento;
    this.evento2.idUsuario = this.evento.id_usuario;
    this.evento2.nombreEvento = this.evento.nombre_evento;
    this.evento2.fechaEvento = this.evento.fecha_evento;
    this.evento2.horaInicioEvento = this.evento.hora_inicio_evento;
    this.evento2.horaFinEvento = this.evento.hora_fin_evento;
  }

  limpiarArreglos() {
    this.evento.id_evento = null;
    this.evento.id_usuario = null;
    this.evento.nombre_evento = null;
    this.evento.fecha_evento = null;
    this.evento.hora_inicio_evento = null;
    this.evento.hora_fin_evento = null;

    this.evento2.idEvento = null;
    this.evento2.idUsuario = null;
    this.evento2.nombreEvento = null;
    this.evento2.fechaEvento = null;
    this.evento2.horaInicioEvento = null;
    this.evento2.horaFinEvento = null;

  }

}
