import { Component, OnInit } from '@angular/core';
import { EventoService } from '../../services/evento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-evento',
  templateUrl: './nuevo-evento.page.html',
  styleUrls: ['./nuevo-evento.page.scss'],
})
export class NuevoEventoPage implements OnInit {

  evento = {
    idEvento: null,
    idUsuario: null,
    nombreEvento: null,
    fechaEvento: null,
    horaInicioEvento: null,
    horaFinEvento: null
  }

  constructor(private eventoService: EventoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    }
  }

  crearEvento() {
    console.log(this.evento.idEvento);
    console.log(this.evento.idUsuario);
    console.log(this.evento.nombreEvento);
    console.log(this.evento.fechaEvento);
    console.log(this.evento.horaInicioEvento);
    console.log(this.evento.horaFinEvento);
    this.evento.fechaEvento = this.evento.fechaEvento.substring(0, 10);
    this.evento.horaInicioEvento = this.evento.horaInicioEvento.substring(11, 18);
    this.evento.horaFinEvento = this.evento.horaFinEvento.substring(11, 18);
    this.evento.idUsuario = window.localStorage.getItem("idUsuario");
    console.log(this.evento.idEvento);
    console.log(this.evento.idUsuario);
    console.log(this.evento.nombreEvento);
    console.log(this.evento.fechaEvento);
    console.log(this.evento.horaInicioEvento);
    console.log(this.evento.horaFinEvento);
    this.eventoService.crearEvento1(this.evento).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Evento registrado correctamente");
          this.limpiarArreglo();
          this.router.navigate(['/tabs/tab2']);
        } else if (datos['estado'] == 2) {
          alert("Registro incorrecto");
        }
      }
    );
  }

  limpiarArreglo() {
    this.evento.idEvento = null;
    this.evento.idUsuario = null;
    this.evento.nombreEvento = null;
    this.evento.fechaEvento = null;
    this.evento.horaInicioEvento = null;
    this.evento.horaFinEvento = null;
  }

}
