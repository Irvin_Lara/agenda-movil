import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  usuario = {
    idUsuario: null,
    correoUsuario: null,
    passUsuario: null,
    nombreUsuario: null,
    apellidoPaternoUsuario: null,
    apellidoMaternoUsuario: null,
    fechaNacimientoUsuario: null
  }

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") != null) {
      this.router.navigate(['/tabs']);
    }
  }

  crearUsuario() {
    this.usuario.fechaNacimientoUsuario = this.usuario.fechaNacimientoUsuario.substring(0, 10);
    this.usuarioService.crearUsuario1(this.usuario).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Usuario registrado correctamente");
          this.limpiarArreglo();
          this.router.navigate(['/login']);
        } else if (datos['estado'] == 2) {
          alert("Registro incorrecto");
        }
      }
    );
  }

  limpiarArreglo() {
    this.usuario.idUsuario = null;
    this.usuario.correoUsuario = null;
    this.usuario.passUsuario = null;
    this.usuario.nombreUsuario = null;
    this.usuario.apellidoPaternoUsuario = null;
    this.usuario.apellidoMaternoUsuario = null;
    this.usuario.fechaNacimientoUsuario = null;
  }

}
