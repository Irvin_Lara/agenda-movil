import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login = {
    correoUsuario: null,
    passUsuario: null
  }

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") != null) {
      this.router.navigate(['/tabs']);
    }
  }

  comprobarLogin() {
    this.usuarioService.comprobarLogin1(this.login).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Login Correcto");
          window.localStorage.setItem("idUsuario", datos['idUsuario']);
          this.limpiarArreglo();
          this.router.navigate(['/tabs']);
        } else if (datos['estado'] == 2) {
          alert("Login Incorrecto");
        }
      }
    );
  }

  limpiarArreglo() {
    this.login.correoUsuario = null;
    this.login.passUsuario = null;
  }

}
