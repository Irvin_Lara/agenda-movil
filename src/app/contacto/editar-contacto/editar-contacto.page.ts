import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../services/contacto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-contacto',
  templateUrl: './editar-contacto.page.html',
  styleUrls: ['./editar-contacto.page.scss'],
})
export class EditarContactoPage implements OnInit {

  contacto = {
    id_contacto: null,
    id_usuario: null,
    nombre_contacto: null,
    apellido_paterno_contacto: null,
    apellido_materno_contacto: null,
    telefono_contacto: null,
    correo_contacto: null,
    ciudad_contacto: null,
    direccion_contacto: null,
    fecha_nacimiento_contacto: null
  }

  contacto2 = {
    idContacto: null,
    idUsuario: null,
    nombreContacto: null,
    apellidoPaternoContacto: null,
    apellidoMaternoContacto: null,
    telefonoContacto: null,
    correoContacto: null,
    ciudadContacto: null,
    direccionContacto: null,
    fechaNacimientoContacto: null
  }

  constructor(private contactoService: ContactoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      //alert("hola");
      this.obtenerContacto();
    }
  }

  obtenerContacto() {
    this.contactoService.obtenerContacto1(window.localStorage.getItem("idContacto")).subscribe(
      (data:any)=> {this.contacto = data.contacto;},
      (error)=> {console.log(error);}
    );
  }

  editarContacto() {
    this.contacto.fecha_nacimiento_contacto = this.contacto.fecha_nacimiento_contacto.substring(0, 10);
    this.contacto.id_usuario = window.localStorage.getItem("idUsuario");
    this.asignarArreglo();
    this.contactoService.actualizarContacto1(this.contacto2).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Contacto actualizado correctamente");
          this.limpiarArreglos();
          this.router.navigate(['/ver-contacto']);
        } else if (datos['estado'] == 2) {
          alert("Registro incorrecto");
        }
      }
    );
  }

  asignarArreglo() {
    this.contacto2.idContacto = this.contacto.id_contacto;
    this.contacto2.idUsuario = this.contacto.id_usuario;
    this.contacto2.nombreContacto = this.contacto.nombre_contacto;
    this.contacto2.apellidoPaternoContacto = this.contacto.apellido_paterno_contacto;
    this.contacto2.apellidoMaternoContacto = this.contacto.apellido_materno_contacto;
    this.contacto2.telefonoContacto = this.contacto.telefono_contacto;
    this.contacto2.correoContacto = this.contacto.correo_contacto;
    this.contacto2.ciudadContacto = this.contacto.ciudad_contacto;
    this.contacto2.direccionContacto = this.contacto.direccion_contacto;
    this.contacto2.fechaNacimientoContacto = this.contacto.fecha_nacimiento_contacto;
  }

  limpiarArreglos() {
    this.contacto.id_contacto = null;
    this.contacto.id_usuario = null;
    this.contacto.nombre_contacto = null;
    this.contacto.apellido_paterno_contacto = null;
    this.contacto.apellido_materno_contacto = null;
    this.contacto.telefono_contacto = null;
    this.contacto.correo_contacto = null;
    this.contacto.ciudad_contacto = null;
    this.contacto.direccion_contacto = null;
    this.contacto.fecha_nacimiento_contacto = null;

    this.contacto2.idContacto = null;
    this.contacto2.idUsuario = null;
    this.contacto2.nombreContacto = null;
    this.contacto2.apellidoPaternoContacto = null;
    this.contacto2.apellidoMaternoContacto = null;
    this.contacto2.telefonoContacto = null;
    this.contacto2.correoContacto = null;
    this.contacto2.ciudadContacto = null;
    this.contacto2.direccionContacto = null;
    this.contacto2.fechaNacimientoContacto = null;
  }

}
