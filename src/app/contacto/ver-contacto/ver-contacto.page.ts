import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../services/contacto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-contacto',
  templateUrl: './ver-contacto.page.html',
  styleUrls: ['./ver-contacto.page.scss'],
})
export class VerContactoPage implements OnInit {

  contacto = {
    id_contacto: null,
    id_usuario: null,
    nombre_contacto: null,
    apellido_paterno_contacto: null,
    apellido_materno_contacto: null,
    telefono_contacto: null,
    correo_contacto: null,
    ciudad_contacto: null,
    direccion_contacto: null,
    fecha_nacimiento_contacto: null
  }

  constructor(private contactoService: ContactoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      //alert("hola");
      this.obtenerContacto();
    }
  }

  obtenerContacto() {
    this.contactoService.obtenerContacto1(window.localStorage.getItem("idContacto")).subscribe(
      (data:any)=> {this.contacto = data.contacto;},
      (error)=> {console.log(error);}
    );
  }

  editarContacto() {
    this.router.navigate(['/editar-contacto']);
  }

  eliminarContacto() {
    this.contactoService.eliminarContacto1(window.localStorage.getItem("idContacto")).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Contacto eliminado correctamente");
          this.router.navigate(['/tabs/tab1']);
        } else if (datos['estado'] == 2) {
          alert("Eliminación incorrecta");
        }
      }
    );
  }

}
