import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../services/contacto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-contacto',
  templateUrl: './nuevo-contacto.page.html',
  styleUrls: ['./nuevo-contacto.page.scss'],
})
export class NuevoContactoPage implements OnInit {

  contacto = {
    idContacto: null,
    idUsuario: null,
    nombreContacto: null,
    apellidoPaternoContacto: null,
    apellidoMaternoContacto: null,
    telefonoContacto: null,
    correoContacto: null,
    ciudadContacto: null,
    direccionContacto: null,
    fechaNacimientoContacto: null
  }

  constructor(private contactoService: ContactoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    }
  }

  crearContacto() {
    this.contacto.fechaNacimientoContacto = this.contacto.fechaNacimientoContacto.substring(0, 10);
    this.contacto.idUsuario = window.localStorage.getItem("idUsuario");
    this.contactoService.crearContacto1(this.contacto).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Contacto registrado correctamente");
          this.limpiarArreglo();
          this.router.navigate(['/tabs/tab1']);
        } else if (datos['estado'] == 2) {
          alert("Registro incorrecto");
        }
      }
    );
  }

  limpiarArreglo() {
    this.contacto.idContacto = null;
    this.contacto.idUsuario = null;
    this.contacto.nombreContacto = null;
    this.contacto.apellidoPaternoContacto = null;
    this.contacto.apellidoMaternoContacto = null;
    this.contacto.telefonoContacto = null;
    this.contacto.correoContacto = null;
    this.contacto.ciudadContacto = null;
    this.contacto.direccionContacto = null;
    this.contacto.fechaNacimientoContacto = null;
  }

}
