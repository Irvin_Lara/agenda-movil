import { Component, OnInit } from '@angular/core';
import { EventoService } from '../services/evento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  eventos = null;

  constructor(private eventoService: EventoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      //alert("hola");
      this.obtenerEventos();
    }
  }

  obtenerEventos() {
    this.eventoService.obtenerEventos1(window.localStorage.getItem("idUsuario")).subscribe(
      (data:any)=> {this.eventos = data.eventos;},
      (error)=> {console.log(error);}
    );
  }

  editarEvento(idEvento: string) {
    window.localStorage.setItem("idEvento", idEvento);
    this.router.navigate(['/editar-evento']);
  }

  eliminarEvento(idEvento: string) {
    this.eventoService.eliminarEvento1(idEvento).subscribe(
      datos => {
        if(datos['estado'] == 1) {
          alert("Evento eliminado correctamente");
          this.router.navigate(['/tabs/tab2']);
        } else if (datos['estado'] == 2) {
          alert("Eliminación incorrecta");
        }
      }
    );
  }

}
