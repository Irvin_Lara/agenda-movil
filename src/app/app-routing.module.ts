import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './login/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './login/register/register.module#RegisterPageModule' },
  { path: 'nuevo-contacto', loadChildren: './contacto/nuevo-contacto/nuevo-contacto.module#NuevoContactoPageModule' },
  { path: 'ver-contacto', loadChildren: './contacto/ver-contacto/ver-contacto.module#VerContactoPageModule' },
  { path: 'editar-contacto', loadChildren: './contacto/editar-contacto/editar-contacto.module#EditarContactoPageModule' },
  { path: 'editar-evento', loadChildren: './evento/editar-evento/editar-evento.module#EditarEventoPageModule' },
  { path: 'nuevo-evento', loadChildren: './evento/nuevo-evento/nuevo-evento.module#NuevoEventoPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
