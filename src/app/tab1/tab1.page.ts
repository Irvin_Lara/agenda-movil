import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../services/contacto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  contactos = null;

  palabra = "";

  constructor(private contactoService: ContactoService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      //alert("hola");
      this.obtenerContactos();
    }
  }

  obtenerContactos() {
    this.contactoService.obtenerContactos1(window.localStorage.getItem("idUsuario")).subscribe(
      (data:any)=> {this.contactos = data.contactos;},
      (error)=> {console.log(error);}
    );
  }

  verContacto(idContacto: string) {
    window.localStorage.setItem("idContacto", idContacto);
    this.router.navigate(['/ver-contacto']);
  }

  buscar() {
    if (this.palabra == "") {
      this.obtenerContactos();
    } else {
      this.contactoService.buscarContactos1(this.palabra).subscribe(
        (data:any)=> {this.contactos = data.contactos;},
        (error)=> {console.log(error);}
      );
    }

  }

  limpiar() {
    this.obtenerContactos();
  }

}
