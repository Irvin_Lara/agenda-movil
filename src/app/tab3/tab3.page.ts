import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  usuario = {
    id_usuario: null,
    correo_usuario: null,
    pass_usuario: null,
    nombre_usuario: null,
    apellido_paterno_usuario: null,
    apellido_materno_usuario: null,
    fecha_nacimiento_usuario: null
  }

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (window.localStorage.getItem("idUsuario") == null) {
      alert("No tiene permiso, inicie sesión");
      this.router.navigate(['/login']);
    } else {
      this.obtenerUsuario();
    }
  }

  obtenerUsuario() {
    this.usuarioService.obtenerUsuario1(window.localStorage.getItem("idUsuario")).subscribe(
      (data:any)=> {this.usuario = data.usuario;},
      (error)=> {console.log(error);}
    );
  }

  cerrarSesion() {
    window.localStorage.removeItem("idUsuario");
    this.router.navigate(['/login']);
  }

}
