import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  URL = "http://195.231.5.11/web-services/"

  constructor(private http: HttpClient) { }

  obtenerUsuarios1() {
    return this.http.get(`${this.URL}obtener_usuarios.php`);
  }

  obtenerUsuario1(idUsuario: string) {
    return this.http.get(`${this.URL}obtener_usuario.php?idUsuario=${idUsuario}`);
  }

  comprobarLogin1(login) {
    return this.http.post(`${this.URL}comprobar_login.php`, JSON.stringify(login));
  }

  crearUsuario1(usuario) {
    return this.http.post(`${this.URL}crear_usuario.php`, JSON.stringify(usuario));
  }

  actualizarUsuario1(usuario) {
    return this.http.post(`${this.URL}actualizar_usuario.php`, JSON.stringify(usuario));
  }

  eliminarUsuario1(idUsuario: string) {
    return this.http.post(`${this.URL}eliminar_usuario.php`, JSON.stringify(idUsuario));
  }

}
