import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  URL = "http://195.231.5.11/web-services/"

  constructor(private http: HttpClient) { }

  obtenerEventos1(idUsuario: string) {
    return this.http.get(`${this.URL}obtener_eventos.php?idUsuario=${idUsuario}`);
  }

  obtenerEvento1(idEvento: string) {
    return this.http.get(`${this.URL}obtener_evento.php?idEvento=${idEvento}`);
  }

  crearEvento1(evento) {
    return this.http.post(`${this.URL}crear_evento.php`, JSON.stringify(evento));
  }

  actualizarEvento1(evento) {
    return this.http.post(`${this.URL}actualizar_evento.php`, JSON.stringify(evento));
  }

  eliminarEvento1(idEvento: string) {
    return this.http.post(`${this.URL}eliminar_evento.php`, JSON.stringify(idEvento));
  }

}
