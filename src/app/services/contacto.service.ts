import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  URL = "http://195.231.5.11/web-services/"

  constructor(private http: HttpClient) { }

  obtenerContactos1(idUsuario: string) {
    return this.http.get(`${this.URL}obtener_contactos.php?idUsuario=${idUsuario}`);
  }

  obtenerContacto1(idContacto: string) {
    return this.http.get(`${this.URL}obtener_contacto.php?idContacto=${idContacto}`);
  }

  buscarContactos1(palabra: string) {
    return this.http.get(`${this.URL}buscar_contactos.php?word=${palabra}`);
  }

  crearContacto1(contacto) {
    return this.http.post(`${this.URL}crear_contacto.php`, JSON.stringify(contacto));
  }

  actualizarContacto1(contacto) {
    return this.http.post(`${this.URL}actualizar_contacto.php`, JSON.stringify(contacto));
  }

  eliminarContacto1(idContacto: string) {
    return this.http.post(`${this.URL}eliminar_contacto.php`, JSON.stringify(idContacto));
  }

}
